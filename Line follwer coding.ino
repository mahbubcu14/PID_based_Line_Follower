#define M1_DEFAULT_SPEED 70
#define M2_DEFAULT_SPEED 56
#define M1_MAX_SPEED 150
#define M2_MAX_SPEED 140
float Kp = 0.45, Kd =4;
int P=0,D=0;
int sensor[6] = {0,0, 0, 0, 0, 0};
void read_sensor(void);
void zigzag(void);


void setup()
{
 
 pinMode(5, OUTPUT); //PWM Pin 1
 pinMode(11, OUTPUT); //PWM Pin 2
 pinMode(7, OUTPUT); //Left Motor Pin 1
 pinMode(8, OUTPUT); //Left Motor Pin 2
 pinMode(2, OUTPUT); //Right Motor Pin 1
 pinMode(3, OUTPUT); //Right Motor Pin 2
 Serial.begin(9600); //Enable Serial Communications
 delay(7000);
 set_motors(180,150);
 delay(500);
}

int error = 0;
int lastError=0;
void loop()
{ 
  read_sensor_error();
  zigzag();
  P=error;
  D=error - lastError;

  int motorSpeed = Kp * P + Kd * D;
  int lastError = error;

  int leftMotorSpeed = M1_DEFAULT_SPEED + motorSpeed;
  int rightMotorSpeed = M2_DEFAULT_SPEED - motorSpeed;
  set_motors(leftMotorSpeed, rightMotorSpeed);

  Serial.print("Left Motor: ");
  Serial.print(leftMotorSpeed);
  Serial.print(" Right Motor: ");
  Serial.print(rightMotorSpeed);
  Serial.println();
}


void read_sensor_error(){

  sensor[0] = analogRead(A0);
    if (sensor[0] > 500 /*when sensor closer to ground400*/) sensor[0] = 1;
    else sensor[0] = 0;
    sensor[1] = analogRead(A1);
    if (sensor[1] > 500 /*when sensor closer to ground500*/) sensor[1] = 1;
    else sensor[1] = 0;
    sensor[2] = analogRead(A2);
    if (sensor[2] > 500 /*when sensor closer to ground500*/) sensor[2] = 1;
    else sensor[2] = 0;
    sensor[3] = analogRead(A3);
    if (sensor[3] > 500 /*when sensor closer to ground500*/) sensor[3] = 1;
    else sensor[3] = 0;
    sensor[4] = analogRead(A4);
    if (sensor[4] > 500 /*when sensor closer to ground500*/) sensor[4] = 1;
    else sensor[4] = 0;
    sensor[5] = analogRead(A5);
    if (sensor[5] > 500 /*when sensor closer to ground500*/) sensor[5] = 1;
    else sensor[5] = 0;

    Serial.print(" Sensor Value: ");
    Serial.print(sensor[0]);
    Serial.print(sensor[1]);
    Serial.print(sensor[2]);
    Serial.print(sensor[3]);
    Serial.print(sensor[4]);
    Serial.print(sensor[5]);
    Serial.println();
   
  if ((sensor[0] == 1) && (sensor[1] == 1) && (sensor[2] == 0) && (sensor[3] == 0) && (sensor[4] == 1) && (sensor[5] == 1)) //110011
    error = 0;
  else if ((sensor[0] == 1) && (sensor[1] == 0) && (sensor[2] == 0) && (sensor[3] == 0) && (sensor[4] == 1) && (sensor[5] == 1)) //100011
    error = 2;
  else if ((sensor[0] == 1) && (sensor[1] == 0) && (sensor[2] == 0) && (sensor[3] == 1) && (sensor[4] == 1) && (sensor[5] == 1)) //100111
    error = 1;
  else if ((sensor[0] == 0) && (sensor[1] == 0) && (sensor[2] == 1) && (sensor[3] == 1) && (sensor[4] == 1) && (sensor[5] == 1)) //001111
    error = 3;
  else if ((sensor[0] == 0) && (sensor[1] == 0) && (sensor[2] == 0) && (sensor[3] == 1) && (sensor[4] == 1) && (sensor[5] == 1)) //000111
    error = 4;
  else if ((sensor[0] == 0) && (sensor[1] == 1) && (sensor[2] == 1) && (sensor[3] == 1) && (sensor[4] == 1) && (sensor[5] == 1)) //011111
    error = 6;
  else if ((sensor[0] == 1) && (sensor[1] == 1) && (sensor[2] == 0) && (sensor[3] == 0) && (sensor[4] == 0) && (sensor[5] == 1)) //110001
    error = -2;
  else if ((sensor[0] == 1) && (sensor[1] == 1) && (sensor[2] == 1) && (sensor[3] == 0) && (sensor[4] == 0) && (sensor[5] == 1)) //111001
    error = -1;
  else if ((sensor[0] == 1) && (sensor[1] == 1) && (sensor[2] == 1) && (sensor[3] == 1) && (sensor[4] == 0) && (sensor[5] == 0)) //111100
    error = -3;
  else if ((sensor[0] == 1) && (sensor[1] == 1) && (sensor[2] == 1) && (sensor[3] == 0) && (sensor[4] == 0) && (sensor[5] == 0)) //111000
    error = -4;
  else if ((sensor[0] == 1) && (sensor[1] == 1) && (sensor[2] == 1) && (sensor[3] == 1) && (sensor[4] == 1) && (sensor[5] == 0)) //111110
    error = -6;
}

void zigzag(){
  sensor[0] = analogRead(A0);
    if (sensor[0] > 500 /*when sensor closer to ground400*/) sensor[0] = 1;
    else sensor[0] = 0;
    sensor[1] = analogRead(A1);
    if (sensor[1] > 500 /*when sensor closer to ground500*/) sensor[1] = 1;
    else sensor[1] = 0;
    sensor[2] = analogRead(A2);
    if (sensor[2] > 500 /*when sensor closer to ground500*/) sensor[2] = 1;
    else sensor[2] = 0;
    sensor[3] = analogRead(A3);
    if (sensor[3] > 500 /*when sensor closer to ground500*/) sensor[3] = 1;
    else sensor[3] = 0;
    sensor[4] = analogRead(A4);
    if (sensor[4] > 500 /*when sensor closer to ground500*/) sensor[4] = 1;
    else sensor[4] = 0;
    sensor[5] = analogRead(A5);
    if (sensor[5] > 500 /*when sensor closer to ground500*/) sensor[5] = 1;
    else sensor[5] = 0;



   //When all the sensors are on the black Line 
   // This condition applied for the junction of a loop
    if ((sensor[0] == 0) && (sensor[1] == 0) && (sensor[2] == 0) && (sensor[3] == 0) && (sensor[4] == 0) && (sensor[5] == 0)){
      analogWrite(5,70);
      analogWrite(11,70);
      delay(200); 
    }

    
}

void set_motors(int motor1speed, int motor2speed)
{
  if (motor1speed > M1_MAX_SPEED ) motor1speed = M1_MAX_SPEED; // limit top speed
  if (motor2speed > M2_MAX_SPEED ) motor2speed = M2_MAX_SPEED; // limit top speed
  if (motor1speed < 0) motor1speed = 0; // keep motor above 0
  if (motor2speed < 0) motor2speed = 0; // keep motor speed above 0
  digitalWrite(7, HIGH);
  digitalWrite(8, LOW);
  digitalWrite(2, HIGH);
  digitalWrite(3, LOW);
  analogWrite(5,motor1speed);
  analogWrite(11,motor2speed);
  
 
}


