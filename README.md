So here is the documentation for the Line follower we have made.This is a so basic on line follower. 
My aim is to explain the code we made so that the learners can easily understand the basic. 

<h4 style="color:skyblue">Defining the constants and functions</h4>
<br>
<code>#define M1_DEFAULT_SPEED 70
#define M2_DEFAULT_SPEED 56
#define M1_MAX_SPEED 150
#define M2_MAX_SPEED 140
float Kp = 0.45, Kd =4;
int P=0,D=0;
int sensor[6] = {0,0, 0, 0, 0, 0};
void read_sensor(void);
void zigzag(void);
</code>
<br>
In this very first part of the code <code>#define M1_DEFAULT_SPEED 70</code> and <code>#define M2_DEFAULT_SPEED 56</code> define the constant left motor speed and the right motor speed respectively.
The numbers 70 and 56 represent the amount of PWM applied to the motor consantly when the are in a straight line without any error detected by the sensors array. The PWM values can be changed 
whithin the range from 0 to 255 which controls the speed of motors. 

Notice that there is a bit difference between the left motor speed and the right motor speed in the code. This because I noticed that the left motor was much slower than the right 
motor. It was around 26%-27% speed difference between the two motors.So we can call it a mechanical error. To overcome this error I had set a low the PWM for right motor after calibration so that right motor might slow down and 
and got the same speed as the left motor.